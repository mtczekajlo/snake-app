#!/bin/bash
set -Eeuxo pipefail

cd "$(git rev-parse --show-toplevel)"

cross build --target aarch64-unknown-linux-gnu
