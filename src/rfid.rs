use rfid_rs::{Uid, MFRC522};
use rppal::gpio::{Gpio, InputPin, OutputPin};
use spidev::{SpiModeFlags, Spidev, SpidevOptions};
use std::error::Error;

pub struct Rfid {
    pub irq: InputPin,
    _rst: OutputPin,
    mfrc: MFRC522,
}

impl Rfid {
    pub fn new() -> Result<Rfid, Box<dyn Error>> {
        let mut spi = Spidev::open("/dev/spidev0.0")?;
        let options = SpidevOptions::new()
            .bits_per_word(8)
            .max_speed_hz(8_000_000)
            .mode(SpiModeFlags::SPI_MODE_0)
            .build();
        spi.configure(&options)?;
        let mut mfrc = MFRC522 { spi };
        mfrc.init().expect("RFID initialization failed!");
        let g = Gpio::new()?;
        let irq = g.get(4)?.into_input();
        let mut rst = g.get(7)?.into_output();
        rst.set_high();
        Ok(Rfid {
            irq,
            _rst: rst,
            mfrc,
        })
    }

    pub fn read_uid(&mut self) -> Result<Uid, rfid_rs::Error> {
        self.mfrc.new_card_present()?;
        self.mfrc.read_card_serial()
    }
}
