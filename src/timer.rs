use std::time::{Duration, Instant};

pub struct Timer {
    started: bool,
    timepoint: Instant,
    duration: Duration,
}

impl Timer {
    pub fn new() -> Timer {
        Timer {
            started: false,
            timepoint: Instant::now(),
            duration: Duration::from_secs(1),
        }
    }

    pub fn for_seconds(&mut self, seconds: u64) {
        self.duration = Duration::from_secs(seconds);
    }

    pub fn start(&mut self) {
        self.started = true;
        self.timepoint = Instant::now();
    }

    pub fn stop(&mut self) {
        self.started = false;
    }

    pub fn is_started(&self) -> bool {
        self.started
    }

    // pub fn is_running(&self) -> bool {
    //     !self.is_done()
    // }

    pub fn is_done(&self) -> bool {
        self.is_started() && ((Instant::now() - self.timepoint) > self.duration)
    }
}
