use rppal::gpio::{Gpio, InputPin};

pub const DOOR_NAME: &str = "drzwi";
pub const SOCIAL_LEFT_NAME: &str = "okno socjal lewe";
pub const SOCIAL_RIGHT_NAME: &str = "okno socjal prawe";
pub const LAB_LEFT_NAME: &str = "okno labo lewe";
pub const LAB_RIGHT_NAME: &str = "okno labo prawe";
pub struct Sensor {
    name: &'static str,
    pin: InputPin,
    old_state: bool,
}

impl Sensor {
    pub fn new_lab_left() -> Result<Sensor, String> {
        Sensor::new(LAB_LEFT_NAME, 23)
    }

    pub fn new_lab_right() -> Result<Sensor, String> {
        Sensor::new(LAB_RIGHT_NAME, 17)
    }

    pub fn new_social_left() -> Result<Sensor, String> {
        Sensor::new(SOCIAL_LEFT_NAME, 27)
    }

    pub fn new_social_right() -> Result<Sensor, String> {
        Sensor::new(SOCIAL_RIGHT_NAME, 22)
    }

    pub fn new_door() -> Result<Sensor, String> {
        Sensor::new(DOOR_NAME, 18)
    }

    pub fn new_set() -> Result<Vec<Sensor>, String> {
        Ok(vec![
            Sensor::new_lab_left()?,
            Sensor::new_lab_right()?,
            Sensor::new_social_left()?,
            Sensor::new_social_right()?,
            Sensor::new_door()?,
        ])
    }

    pub fn new(name: &'static str, gpio_pin: u8) -> Result<Sensor, String> {
        let g = match Gpio::new() {
            Ok(ok) => ok,
            Err(err) => return Err(format!("Error in GPIO access! {}", err)),
        };
        let pin = g.get(gpio_pin).unwrap().into_input_pullup();
        Ok(Sensor {
            name,
            pin,
            old_state: true,
        })
    }

    pub fn is_closed(&self) -> bool {
        self.pin.is_low()
    }

    pub fn has_changed(&mut self) -> bool {
        let is_closed = self.is_closed();
        if self.old_state != is_closed {
            self.old_state = is_closed;
            return true;
        }
        false
    }

    pub fn get_name(&self) -> &str {
        self.name
    }
}
