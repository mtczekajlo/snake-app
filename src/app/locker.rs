use serde::{Deserialize, Serialize};
use serde_json;
use std::error::Error;
use std::fs::*;
use std::io::prelude::*;
use std::path::*;

const DB_FP: &str = "db.json";

#[derive(Clone, Serialize, Deserialize)]
pub struct User {
    pub name: String,
    pub uid: String,
    pub pin: String,
}

#[derive(Serialize, Deserialize)]
pub struct Locker {
    alarm: bool,
    armed: bool,
    last_user: String,
    last_action_time: String,
    users: Vec<User>,
}

impl Locker {
    pub fn default() -> Locker {
        Locker {
            alarm: false,
            armed: false,
            last_user: String::new(),
            last_action_time: String::new(),
            users: vec![
                User {
                    name: String::from("WhiteCard"),
                    uid: String::from("37E372B4"),
                    pin: String::from("1234"),
                },
                User {
                    name: String::from("BlueTag"),
                    uid: String::from("AA82F93F"),
                    pin: String::from("0000"),
                },
            ],
        }
    }

    pub fn read_db() -> Result<Locker, Box<dyn Error>> {
        if Path::new(DB_FP).exists() {
            let mut file = File::open(DB_FP)?;
            let mut content = String::new();
            file.read_to_string(&mut content)?;
            let locker: Locker = serde_json::from_str(&content)?;
            return Ok(locker);
        }
        Err("Baza danych nie istnieje!".into())
    }

    pub fn is_armed(&self) -> bool {
        self.armed
    }

    pub fn is_alarmed(&self) -> bool {
        self.alarm
    }

    pub fn get_last_user(&self) -> String {
        self.last_user.clone()
    }

    pub fn set_last_user(&mut self, last_user: String) {
        self.last_user = last_user;
    }

    pub fn get_last_action_time(&self) -> String {
        self.last_action_time.clone()
    }

    pub fn set_last_action_time(&mut self, last_action_time: String) {
        self.last_action_time = last_action_time;
    }

    pub fn set_armed(&mut self, armed: bool) -> std::io::Result<()> {
        self.armed = armed;
        self.update_db()
    }

    pub fn get_alarm(&self) -> bool {
        self.alarm
    }

    pub fn set_alarm(&mut self, alarm: bool) -> std::io::Result<()> {
        self.alarm = alarm;
        self.update_db()
    }

    pub fn find_user_by_uid(&self, uid: &String) -> Option<User> {
        for user in &self.users {
            if &user.uid == uid {
                return Some(user.clone());
            }
        }
        None
    }

    pub fn find_user_by_name(&self, username: &String) -> Option<User> {
        for user in &self.users {
            if &user.name == username {
                return Some(user.clone());
            }
        }
        None
    }

    pub fn login_attempt(&mut self, uid: &String) -> Result<User, String> {
        match self.find_user_by_uid(uid) {
            Some(user) => Ok(user),
            None => Err(format!("Nieznana karta: {}", uid)),
        }
    }

    pub fn pin_attempt(&self, username: &String, pin: &String) -> Result<(), String> {
        match self.find_user_by_name(username) {
            Some(user) => {
                if pin != &user.pin {
                    return Err(format!("PIN nieprawidłowy!"));
                }
                Ok(())
            }
            None => Err(format!("Nieznany użytkownik: {}", username)),
        }
    }

    fn update_db(&mut self) -> std::io::Result<()> {
        if !Path::new(DB_FP).exists() {
            File::create(DB_FP).unwrap();
        }
        let mut file = OpenOptions::new().write(true).truncate(true).open(DB_FP)?;
        let j = serde_json::to_string(&self)?;
        file.write_all(j.as_bytes())
    }
}
