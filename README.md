# S.N.A.K.E. - System Natychmiastowego Alarmowania o Kradzieży Elektroniki

![S.N.A.K.E.](docs/snake.png)

## Prerequisites

### Git LFS

When you clone this repo, remember to pull binary data (`*.mp3`, `*.png`) using `git lfs`.

### Rust

```bash
❯ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

from: <https://www.rust-lang.org/tools/install>

### Docker/Podman

You will need `docker` or `podman` to build an image that is being used for cross-compilation.

### Cross

```bash
❯ cargo install cross
```

from: <https://github.com/rust-embedded/cross>

## RPi's system

`Ubuntu Mate 20.04` for `RPi 3` (`aarch64`/`arm64` architecture) has been used.

From:

- <https://ubuntu-mate.org/ports/raspberry-pi/>
- <https://ubuntu-mate.org/download/arm64/>

Install and setup the system.

Setup either the `Wi-Fi` connection (use `nmtui`) or simply connect an `ethernet` cable.

If you want to find its `IP` I recommend use `nmap` -> `nmap -sn 192.168.1.0/24`

You can `ssh` to it (if `openSSH` has been enabled) using either:

- `ssh <username>@<ip>` -> `ssh snake@192.168.1.123`
- `ssh <username>@<hostname>.local` -> `ssh snake@rpi.local`

Use `alsamixer` to control audio output.

All next steps perform as a `root`.

Real men don't use GUI - disable it:

```bash
systemctl set-default multi-user.target
```

Create `/etc/rc.local` file, make it executable (`chmod a+x /etc/rc.local`) and fill with:

```/etc/rc.local
#!/bin/bash
chvt 2
```

Enable autologin and app autostart:

```bash
systemctl edit getty@tty2.service
```

Fill with:

```systemd
[Service]
ExecStart=
ExecStart=-/root/snake
WorkingDirectory=/root
StandardOutput=tty
StandardInput=tty
Type=simple
Restart=always
RestartSec=1
```

## Connectivity

### RPi3 B+

![RPi 3B+](docs/Pi3B+_Pinout.png)

### Windows and door sensors

Each `sensor` (windows' and door sensors) has 2 pins that need to be connected to `GND` and:

- lab window left -> `GPIO 23`
- lab window right -> `GPIO 17`
- social window left -> `GPIO 27`
- social window right -> `GPIO 22`
- door -> `GPIO 18`

![Reed sensor](docs/reed_sensor.jpg)

### Keypad

`Keypad` has 8 pins that create a 4x4 matrix:

- PIN `1` -> `GPIO 26` (row 1.)
- PIN `2` -> `GPIO 19` (row 2.)
- PIN `3` -> `GPIO 13` (row 3.)
- PIN `4` -> `GPIO 6` (row 4.)
- PIN `5` -> `GPIO 21` (column 1.)
- PIN `6` -> `GPIO 20` (column 2.)
- PIN `7` -> `GPIO 16` (column 3.)
- PIN `8` -> `GPIO 12` (column 4.)

![Keypad](docs/keypad.jpg)

### RFID

`MFRC 522` uses `SPI` connection and `GPIO` reset and interrupt pins.

- `SPI` -> `SPI 0`
- `interrupt` -> `GPIO 4`
- `reset` -> `GPIO 7`

![MFRC522](docs/RC522-RFID-Pinout.png)

### Audio

Simply plug in an audio jack into `RPi`'s socket.

## Build

### Prepare cross-compile image

Pull and create a base container from image below:

```bash
❯ podman run -it --name snake-builder rustembedded/cross:aarch64-unknown-linux-gnu
```

This should occur:

```bash
root@<random_hash>:/#
```

That means you're in. Within container perform:

```bash
❯ dpkg --add-architecture arm64 && apt update && apt install -y libasound2-dev libasound2-dev:arm64
```

`Exit` from container and `commit` it. It will be converted from container to image:

```bash
❯ podman commit snake-builder snake-builder:latest
```

### Cross-compile

To cross-compile this app perform a debug or release build:

```bash
❯ cross build --target aarch64-unknown-linux-gnu # Debug build
❯ cross build --target aarch64-unknown-linux-gnu --release # Release build
```

Built artifact should be found here:

```bash
❯ find . -name snake
./target/aarch64-unknown-linux-gnu/release/snake
./target/aarch64-unknown-linux-gnu/debug/snake
```

## Upload

To upload app and resources use (from repo root directory):

```bash
❯ rsync -avzhr target/aarch64-unknown-linux-gnu/<build_type>/snake ./sounds <user>@<rpi_ip>:~/.
```

You may need to disable service before upload. For development purposes I recommend to stop service:

```bash
❯ systemctl stop getty@tty2.service
```

It'll be restarted after reboot.

## Run

Go to `RPi` and execute app with either `root` privileges or as a service:

```bash
❯ systemctl restart getty@tty2.service
```
